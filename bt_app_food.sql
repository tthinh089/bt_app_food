CREATE DATABASE bt_app_food

CREATE TABLE user(
user_id int NOT NULL AUTO_INCREMENT,
full_name varchar(255),
email varchar(255),
password varchar(255),
PRIMARY KEY (user_id)
);

CREATE TABLE food(
food_id int NOT NULL AUTO_INCREMENT,
food_name varchar(255),
image varchar(255),
price float,
des varchar(255),
type_id int NOT NULL,
PRIMARY KEY (food_id),
FOREIGN KEY (type_id) REFERENCES food_type(type_id)
);

CREATE TABLE orders(
user_id int NOT NULL,
food_id int NOT NULL,
amount int,
code varchar(255),
arr_sub_id varchar(255),
FOREIGN KEY (user_id) REFERENCES user(user_id),
FOREIGN KEY (food_id) REFERENCES food(food_id)
);

CREATE TABLE sub_food(
sub_id int NOT NULL AUTO_INCREMENT,
sub_name varchar(255),
sub_price float,
food_id int NOT NULL,
PRIMARY KEY (sub_id),
FOREIGN KEY (food_id) REFERENCES food(food_id)
);

CREATE TABLE rate_res(
user_id int NOT NULL,
res_id int NOT NULL,
amount int,
date_rate datetime,
FOREIGN KEY (user_id) REFERENCES user(user_id),
FOREIGN KEY (res_id) REFERENCES restaurant(res_id)
);

CREATE TABLE restaurant(
res_id int NOT NULL AUTO_INCREMENT,
res_name varchar(255),
Image varchar(255),
des varchar(255),
PRIMARY KEY (res_id)
);

CREATE TABLE like_res(
user_id int NOT NULL,
res_id int NOT NULL,
date_like datetime,
FOREIGN KEY (user_id) REFERENCES user(user_id),
FOREIGN KEY (res_id) REFERENCES restaurant(res_id)
);


CREATE TABLE food_type(
type_id int NOT NULL AUTO_INCREMENT,
type_name varchar(255),
PRIMARY KEY (type_id)
);

INSERT INTO rate_res (user_id, res_id, amount, date_rate) VALUES
  (1, 1, 5, '2023-08-05'),
  (2, 2, 4, '2023-08-12'),
  (3, 3, 3, '2023-08-23'),
  (4, 4, 2, '2023-08-24'),
  (5, 5, 1, '2023-08-25'),
  (6, 6, 5, '2023-08-28'),
  (7, 7, 4, '2023-08-29'),
  (8, 8, 3, '2023-09-06'),
  (9, 9, 2, '2023-09-14'),
  (10, 10, 1, '2023-09-15');
  
  INSERT INTO user (full_name, email, password) VALUES
  ('John Doe', 'john.doe@example.com', 'password'),
  ('Jane Doe', 'jane.doe@example.com', 'password'),
  ('Peter Smith', 'peter.smith@example.com', 'password'),
  ('Mary Jones', 'mary.jones@example.com', 'password'),
  ('David Williams', 'david.williams@example.com', 'password'),
  ('Emily Johnson', 'emily.johnson@example.com', 'password'),
  ('Michael Brown', 'michael.brown@example.com', 'password'),
  ('Sarah Miller', 'sarah.miller@example.com', 'password'),
  ('William Taylor', 'william.taylor@example.com', 'password'),
  ('Susan Walker', 'susan.walker@example.com', 'password');
  
INSERT INTO orders (user_id, food_id, amount, code, arr_sub_id) VALUES
  (2, 11, 1, 'ORD123', '1,2'),
  (2, 12, 2, 'ORD456', '3,4'),
  (1, 13, 3, 'ORD789', '5,6'),
  (4, 14, 4, 'ORDABC', '7,8'),
  (5, 15, 5, 'ORDDEF', '9,10'),
  (6, 16, 6, 'ORDGHI', '11,12'),
  (5, 17, 7, 'ORDJKL', '13,14'),
  (8, 18, 8, 'ORDMNO', '15,16'),
  (5, 19, 9, 'ORDPQR', '17,18'),
  (4, 20, 10, 'ORDSTU', '19,20');

INSERT INTO food (food_name, image, price, des, type_id) VALUES
  ('Pizza', 'pizza.jpg', 10, 'Delicious pizza with pepperoni.', 1),
  ('Pasta', 'pasta.jpg', 8, 'Creamy spaghetti with tomato sauce.', 2),
  ('Burger', 'burger.jpg', 12, 'Juicy cheeseburger with cheese.', 3),
  ('Fried Rice', NULL, 12, 'Delicious fried rice with chicken.', 1),
  ('Salad', NULL, 10, 'Fresh Caesar salad with romaine lettuce, croutons, Parmesan cheese, and Caesar dressing.', 2),
  ('Soup', NULL, 6, 'Hearty tomato soup with cream and croutons.', 3),
  ('Sandwich', NULL, 8, 'Classic BLT sandwich with bacon, lettuce, and tomato.', 1),
  ('Tacos', NULL, 10, 'Delicious chicken tacos with all the fixings.', 2),
  ('Sushi', NULL, 12, 'Fresh California roll with avocado, cucumber, and crab.', 3),
  ('Dessert', NULL, 10, 'Rich and moist chocolate cake with chocolate frosting.', 1);

INSERT INTO restaurant (res_name, image, des) VALUES
  ('Pizza Hut', 'pizza_hut.jpg', 'Americas favorite pizza chain.'),
  ('Olive Garden', 'olive_garden.jpg', 'Italian restaurant with a wide variety of pasta dishes.'),
  ('McDonalds', 'mcdonalds.jpg', 'Fast food restaurant known for its burgers, fries, and milkshakes.'),
  ('Chipotle', 'chipotle.jpg', 'Mexican fast food restaurant with a focus on fresh, quality ingredients.'),
  ('Subway', 'subway.jpg', 'Sandwich shop with a variety of customizable sandwiches.'),
  ('Taco Bell', 'taco_bell.jpg', 'Mexican fast food restaurant known for its tacos, burritos, and quesadillas.'),
  ('Starbucks', 'starbucks.jpg', 'Coffee shop chain with a variety of coffee drinks, pastries, and other food items.'),
  ('Panera Bread', 'panera_bread.jpg', 'Bakery-cafe chain with a variety of sandwiches, salads, and soups.'),
  ('Five Guys', 'five_guys.jpg', 'Burgers and fries restaurant known for its fresh, made-to-order burgers.'),
  ('In-N-Out Burger', 'in_n_out_burger.jpg', 'Burgers and fries restaurant known for its simple, yet delicious burgers.');
  
  INSERT INTO sub_food (sub_name, sub_price, food_id) VALUES
  ('Pepperoni', 20, 11),
  ('Sausage', 30, 12),
  ('Mushrooms', 10, 13),
  ('Onions', 50, 14),
  ('Green peppers', 10, 15),
  ('Black olives', 30, 16),
  ('Extra cheese', 10, 19),
  ('Spinach', 10, 12),
  ('Artichoke hearts', 10, 13),
  ('Pineapple', 30, 11);
  
  INSERT INTO like_res (user_id, res_id) VALUES
  (1, 1),
  (2, 2),
  (3, 3),
  (4, 4),
  (5, 5),
  (6, 6),
  (7, 7),
  (8, 8),
  (9, 9),
  (10, 10);
  
  INSERT INTO rate_res (user_id, res_id, amount, date_rate) VALUES
  (1, 1, 5, '2023-09-29 20:36:42'),
  (2, 2, 4, '2023-09-29 17:36:42'),
  (3, 3, 3, '2023-09-29 17:36:42'),
  (4, 4, 2, '2023-09-29 22:36:42'),
  (5, 5, 1, '2023-09-29 21:36:42'),
  (6, 6, 5, '2023-09-29 23:36:42'),
  (7, 7, 4, '2023-09-29 22:36:42'),
  (8, 8, 3, '2023-09-29 03:36:42'),
  (9, 9, 2, '2023-09-29 02:36:42'),
  (10, 10, 1, '2023-09-29 11:36:42');
  

-- Tìm 5 người đã like nhà hàng nhiều nhất
SELECT user.user_id, user.full_name , SUM(rate_res.amount) as luot_like
FROM user 
INNER JOIN rate_res
ON user.user_id = rate_res.user_id
INNER JOIN restaurant
ON rate_res.res_id = restaurant.res_id 
GROUP BY user.user_id
ORDER BY luot_like DESC
LIMIT 5;

-- Tìm 2 Nhà hàng có nhiều lượt like nhất:
SELECT restaurant.res_id, restaurant.res_name, SUM(rate_res.amount) as total_like
FROM restaurant 
INNER JOIN rate_res
ON restaurant.res_id = rate_res.res_id
INNER JOIN user
ON user.user_id = rate_res.user_id 
GROUP BY restaurant.res_id
ORDER BY total_like DESC
LIMIT 2;

-- Tìm người đặt hàng nhiều nhất
SELECT user.full_name as name , orders.amount as dat_hang
FROM user
INNER JOIN orders
ON user.user_id = orders.user_id
INNER JOIN food
ON orders.food_id = food.food_id
ORDER BY orders.amount DESC
LIMIT 1;

-- Tìm người dùng không hoạt động trong hệ thống
SELECT user.full_name as name
FROM rate_res
RIGHT JOIN orders
ON rate_res.user_id = orders.user_id
RIGHT JOIN user
ON orders.user_id = user.user_id
WHERE rate_res.amount  IS NULL AND orders.amount IS NULL;